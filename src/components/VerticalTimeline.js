import React from 'react';
import "./VerticalTimeline.css";
import cl from 'classnames';


const defaultProps = {
  name: 'World',
  message: 'This is a simple component.'
};

export default function VerticalTimeline(props) {
    const Icon = ()=> <div className="icon">
      <div style={{width: '20px', background: 'red', height: '20px'}}></div>
    </div>;
    const Heading = (headingProps)=> {
      const {text, icon} = headingProps;
      return <div className="itemcontainer">
        {icon !== undefined ? <Icon />: null}
        {text}
      </div>;
    }

    const TimeLineItem = (timeLineItemProps)=>{

      const {children} = timeLineItemProps;
      console.log(children);
      const items = children ? children.map(
        (child, index, caller)=>{
          const lastItem = (index === caller.length -1);

          if (child.type.name == "TimeLineItem"){
            return <div className="finalitemparents">
                  <div className="finalitemcolumns">
                    <div className="top" />
                    <div className="right" />
                    <div className="left" />
                    <div className="bottom" />
                  </div>
                <div className="itemcontainer">
                  <div className="tierspacer"/>
                  {child}
                </div>
              </div>
          }

          return <li className="item">
              {child}
              {!lastItem ? <div className="items"/> : null}
            </li>
      }) : null;
      return <ul>{items}</ul>;
    };

    const { name, message, children } = props;

    const LastItem = lastitemProps =>(
      <div className="finalitemparent">
        <div className="finalitemcolumn">
          <div className="topright" />
          <div className="bottomright" />
        </div>
        <div>
          <div className="itemcontainer">
            <div className="tierspacer" />
              <Heading icon="I" text="Database Insertion Failure"/>
          </div>
        </div>
      </div>
    )

    return <div>
      <TimeLineItem align="center">
          <Heading icon="1" text="8/19/2017"/>
          <Heading icon="2" text="8/19/2017"/>
          <TimeLineItem>
            <Heading icon="1" text="Sub Tier"/>
            <TimeLineItem>
                <Heading icon="1" text="Sub Tier"/>
                <Heading icon="2" text="Sub Tier"/>
              </TimeLineItem>
          </TimeLineItem>
      </TimeLineItem>

      <div className="items"/>
      <div className="subheading">
        <Heading icon="I" text="10:34 AM"/>
      </div>
      <LastItem />
    </div>
}
