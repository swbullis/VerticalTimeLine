import React from 'react';
import ReactDOM from 'react-dom';
import VerticalTimeline from './components/VerticalTimeline';

ReactDOM.render(
  <VerticalTimeline />,
  document.getElementById('root')
);
